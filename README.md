RaspberryPi Hat for Muon Hodoscope
---

Some more documentation can be found in the wiki: 
https://git.rwth-aachen.de/groups/muon-hodoscope/-/wikis/home#raspberry-pi-hat

![Rendering](exports/hodoscope-hat.png)

## Legal
**NoAI**: This project may not be used in datasets for, in the development of, or as inputs to generative AI programs.

**§44b**: Dies ist ein in maschinenlesbarer Form vorliegender Nutzungsvorbehalt entsprechend §44b UrhG

**License**: CERN-OHL-S [license-text](LICENSE)
* 2024 cpresser
* 2024 Erik Ehlert

## Versions
At least two versions have been produced
* Initial revision, used for T20 (3 pieces manufactured), can be identified by the big circular hole for a fan in the center (https://git.rwth-aachen.de/muon-hodoscope/rpi-hat/-/tags/Order_Aisler_4910278120)
* Updated version with 4 clock outputs (https://git.rwth-aachen.de/muon-hodoscope/rpi-hat/-/tags/Order_Aisler_4910315824)
